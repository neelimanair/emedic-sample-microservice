# Sample SpringBoot Repo

## Get Started
To get started, clone this repo and make sure you have [IntelliJ](https://www.jetbrains.com/idea/) or Eclipse installed. The community edition would suffice. 
Once IntelliJ or Eclipse is installed, ensure that you have the [Lombok](https://plugins.jetbrains.com/plugin/6317-lombok) plugin installed as well to facilitate logging.

Rather than creating a new project in IntelliJ or Eclipse, import this project and make sure to select Maven as the dependency management. Ensure that the option <b>Import Maven Dependencies Automatically</b>
is checked.

Once the above steps have been done, you can start with development.

## Running the Service
There are 3 ways to run the service.

### IntelliJ
There are two ways about this.

1. Create a new run configuration under Maven. Supply the following arguments in the command-line field: `spring-boot:run`
2. Locate the class `SampleApplication.java` and hit the run button.

The service will start up and listen to port `8080` on `localhost`.

### Locally
Open terminal/bash and ensure that you're in the root folder of this project. Make sure that you're on the same level as the pom file.

Run this command and the service should start up and listen to port 8080.

```
mvn spring-boot:run
```

### Docker
Ensure that you have Docker installed. The first step is to ensure that you have dockerized the application.

1. Run the command `mvn package -B` to package the application along with the dependencies
2. Dockerize the application by running the command `docker build -t <image name> .`

Once step two has been performed, you can list all the available Docker images you have locally via `docker image ls`.

To run the application locally on port 8080, supply the following command.
```text
docker run -p 8080:8080 <image id>
```
