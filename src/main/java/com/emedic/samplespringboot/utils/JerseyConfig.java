package com.emedic.samplespringboot.utils;

import com.emedic.samplespringboot.service.SampleService;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

/**
 * @author leon
 * created on 2019-06-29
 */
@Component
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        register(SampleService.class);
    }
}
