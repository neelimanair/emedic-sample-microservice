package com.emedic.samplespringboot.objects.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author neelima
 *
 */

@NamedQueries({
    @NamedQuery(
        name = "findSampleById",
        query = "from Sample a where a.sampleId = :id"
        ),
})
@Entity
@Table(name ="tbl_sample")
public class Sample {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "sample_id")
	private int sampleId;
	
	
	@Column(name = "description")
	private String description;
	
	

	public int getSampleId() {		
		return sampleId;
	}

	public void setSampleId(int sampleId) {
		this.sampleId = sampleId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
