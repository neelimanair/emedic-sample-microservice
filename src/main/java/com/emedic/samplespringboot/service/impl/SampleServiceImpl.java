package com.emedic.samplespringboot.service.impl;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.emedic.samplespringboot.dao.SampleDao;
import com.emedic.samplespringboot.objects.entity.Sample;
import com.emedic.samplespringboot.objects.requests.SampleRequest;
import com.emedic.samplespringboot.objects.responses.SampleResponse;
import com.emedic.samplespringboot.service.SampleService;

/**
 * 
 * @author neelima
 *
 */
@Service
public class SampleServiceImpl implements SampleService {
	
	@Autowired
	private SampleDao sampleDao;

    @Override
    public Response sample(int sampleId) {
        SampleResponse response = new SampleResponse();
        
        Sample sample = sampleDao.findById(sampleId);
       
        if(sample!= null) {
        	response.setResponse("This is a response from a emedic sample spring boot microservice:"+sample.getDescription());
        }

        return Response.status(Response.Status.ACCEPTED).entity(response).build();
    }
    
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void sample(SampleRequest sampleRequest) {
    	Sample sample = new Sample();
    	//sample.setSampleId(sampleRequest.getSampleId());
        sample.setDescription(sampleRequest.getDescription());
        sampleDao.save(sample);
    }
}
