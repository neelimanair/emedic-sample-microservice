package com.emedic.samplespringboot.service;

import org.springframework.stereotype.Component;

import com.emedic.samplespringboot.objects.requests.SampleRequest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author leon
 * created on 2019-06-29
 */
@Component
@Path("/sample")
public interface SampleService {

	@GET
    @Path("/{sampleId}")
    @Produces({MediaType.APPLICATION_JSON})
    Response sample(@PathParam("sampleId") int sampleId);
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON})
	void sample(SampleRequest sampleRequest);
}
