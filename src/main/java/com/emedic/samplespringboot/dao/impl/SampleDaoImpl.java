package com.emedic.samplespringboot.dao.impl;

import javax.persistence.TypedQuery;

//import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.emedic.samplespringboot.dao.SampleDao;
import com.emedic.samplespringboot.objects.entity.Sample;

import lombok.extern.slf4j.Slf4j;


@Slf4j
@Repository
public class SampleDaoImpl implements SampleDao {
	

	@Autowired
    private SessionFactory sessionFactory;
	
	
	
	@Override
	public Sample findById(int sampleId) {
		Session session = null;
		Sample sample = null;
		try {
		session = this.sessionFactory.openSession();
        TypedQuery<Sample> query = session.getNamedQuery("findSampleById");  
        query.setParameter("id", sampleId);
        sample = query.getSingleResult();
        
        } catch (javax.persistence.PersistenceException e) {
            log.error("DB exception occurred: " + e.getMessage());            
        }finally {
        	session.close();
		}
		return sample;
	}

	@Override
	public void save(Sample sample) {
		if(this.sessionFactory.getCurrentSession() != null)
			this.sessionFactory.getCurrentSession().saveOrUpdate(sample);
	}

}
