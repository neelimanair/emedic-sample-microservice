package com.emedic.samplespringboot.dao;

import org.springframework.stereotype.Repository;

import com.emedic.samplespringboot.objects.entity.Sample;

@Repository
public interface SampleDao {
	
	public Sample findById(int sampleId);
	
	public void save(Sample sample);

}
