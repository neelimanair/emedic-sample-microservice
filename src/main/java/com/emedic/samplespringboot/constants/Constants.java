/**
 * 
 */
package com.emedic.samplespringboot.constants;

/**
 * @author neelima
 *
 */
public class Constants {

    public static final String SERVICE_REQUEST = "SERVICE_REQUEST";
    public static final String SERVICE_RESPONSE = "SERVICE_RESPONSE";

    public static final String STORAGE_BUCKET_PHOTO = "emedic_photo";
    public static final String THUMBNAIL_BUCKET_PHOTO = "emedic_photo_thumbnail";
    public static final String STORAGE_BUCKET_VIDEO = "emedic_video";
    public static final String STORAGE_BUCKET_VOICE = "emedic_voice";

    public static final String COM_MYSQL_JDBC_DRIVER = "com.mysql.jdbc.Driver";
    //public static final String DB_URL = "jdbc:mysql://localhost:3306/emedic?useSSL=false&serverTimezone=UTC";
    //public static final String DB_USERNAME = "phoenix";
    //public static final String DB_PASSWORD = "password";
    //public static final String DB_URL="jdbc:mysql://google/emedic?cloudSqlInstance=emedic-fyp:asia-southeast1-a:emedic&socketFactory=com.google.cloud.sql.mysql.SocketFactory&useSSL=false";
    public static final String DB_URL = "jdbc:mysql://34.87.43.231:3306/emedic?useSSL=false";
    public static final String DB_USERNAME = "root";
    public static final String DB_PASSWORD = "v9khs8xxGaCwBsIK";

}
